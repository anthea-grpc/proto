# Commands

1. Compile .proto (from project root dir)

```sh
$ poetry run python -m grpc_tools.protoc -I . --grpc_python_out=generated/python --python_out=generated/python anthea.proto
```